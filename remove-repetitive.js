export default function removerRepetitive(string) {
    var result = string;
    return result
      .split('')
      .filter(function(item, pos, self) {
        return self.indexOf(item) == pos;
      })
      .join('');
  }
